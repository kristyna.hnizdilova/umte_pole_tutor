package cz.uhk.fim.umte.poletutor.ui.signIn

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import cz.uhk.fim.umte.poletutor.MainActivity
import cz.uhk.fim.umte.poletutor.R
import java.util.*
import kotlin.collections.HashMap

class SigninActivity : AppCompatActivity() {

    lateinit var etEmail: EditText
    lateinit var etConfPass: EditText
    private lateinit var etPass: EditText
    private lateinit var etName: EditText
    private lateinit var btnSignUp: Button
    lateinit var tvRedirectLogin: TextView

    //Create Firebase authentication object
    private lateinit var auth: FirebaseAuth

    //init Firestore for store data
    private lateinit var database: FirebaseFirestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        auth = FirebaseAuth.getInstance()
        // View Bindings
        etEmail = findViewById(R.id.etEmail)
        etConfPass = findViewById(R.id.etPotvrditHeslo)
        etPass = findViewById(R.id.etHeslo)
        btnSignUp = findViewById(R.id.btnRegistrovat)
        etName = findViewById(R.id.etJmnoAPjmen)
        tvRedirectLogin = findViewById(R.id.etAlreadyHaveAccount)

        btnSignUp.setOnClickListener {
            signUpUser()
        }

        // switching from signUp Activity to Login Activity
        tvRedirectLogin.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

    private fun signUpUser() {
        val email = etEmail.text.toString()
        val pass = etPass.text.toString()
        val name = etName.text.toString()
        val confirmPassword = etConfPass.text.toString()

        //Check passwords
        if (email.isBlank() || pass.isBlank() || confirmPassword.isBlank()) {
            Toast.makeText(this, "Email a heslo musí být vyplněné", Toast.LENGTH_SHORT).show()
            return
        }

        if (pass != confirmPassword) {
            Toast.makeText(this, "Hesla nesouhlasí", Toast.LENGTH_SHORT)
                .show()
            return
        }
        //Correct credentials
        auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this) {
            if (it.isSuccessful) {
                writeNewUser(it.result.user!!.uid, name, email)
                Toast.makeText(this, "Uživatel byl úspěšně zaregistrován", Toast.LENGTH_SHORT).show()
                finish()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, it.result.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun writeNewUser(uuid: String, name: String, email: String) {
        val add = HashMap<String,Any>()
        add["name"] = name
        add["email"] = email
        database = FirebaseFirestore.getInstance()
        database.collection("users")
            .document(uuid)
            .set(add)
            .addOnFailureListener {
                Toast.makeText(this,it.message.toString(),Toast.LENGTH_LONG).show()
            }
    }
}