package cz.uhk.fim.umte.poletutor.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cz.uhk.fim.umte.poletutor.R
import cz.uhk.fim.umte.poletutor.dto.CourseItem
import cz.uhk.fim.umte.poletutor.dto.Trick

class TrickListAdapter(
    private val mList: List<Trick>,
    val clickListener: (Trick, Int) -> Unit
) : RecyclerView.Adapter<TrickListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): ViewHolder {
        val view: View = LayoutInflater
            .from(viewGroup.context)
            .inflate(
                R.layout.trick_item,
                viewGroup, false
            )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        viewHolder: ViewHolder,
        position: Int
    ) {
        val childItem: Trick = mList[position]
        viewHolder.ChildItemTitle
            .setText(childItem.name)
        viewHolder.itemView.setOnClickListener { clickListener(childItem, position) }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ChildItemTitle: TextView = itemView.findViewById(
            R.id.trick_name_2
        )

    }
}