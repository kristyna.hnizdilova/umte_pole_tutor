package cz.uhk.fim.umte.poletutor.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.RecycledViewPool
import com.google.firebase.firestore.FirebaseFirestore
import cz.uhk.fim.umte.poletutor.R
import cz.uhk.fim.umte.poletutor.dto.CourseItem

class CourseListAdapter (
            private val ids: List<String>,
            private val mList: List<CourseItem>,
            val clickListener: (CourseItem, Int) -> Unit
        ) :
    RecyclerView.Adapter<CourseListAdapter.ParentViewHolder>() {

    private lateinit var db: FirebaseFirestore
    private val viewPool = RecycledViewPool()
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): ParentViewHolder {
        val view: View = LayoutInflater
            .from(viewGroup.context)
            .inflate(
                R.layout.course_item,
                viewGroup, false
            )
        return ParentViewHolder(view)
    }

    override fun onBindViewHolder(
        parentViewHolder: ParentViewHolder,
        position: Int
    ) {
        db = FirebaseFirestore.getInstance()
        val parentItem: CourseItem = mList[position]
        parentViewHolder.ParentItemTitle.setText(parentItem.courseName)
        parentViewHolder.ParentItemLevel.setText(parentItem.level)
        //star logic
        ids.forEach{
            if(it == parentItem.id)
                parentViewHolder.itemView.findViewById<ImageView>(R.id.imageStar1)
                    .setImageResource(R.drawable.img_star4)
        }

        val layoutManager = LinearLayoutManager(
            parentViewHolder.ChildRecyclerView
                .context,
            LinearLayoutManager.VERTICAL,
            false
        )

        layoutManager.initialPrefetchItemCount = parentItem.tricks.size as Int

        // Create an instance of the child
        // item view adapter and set its
        // adapter, layout manager and RecyclerViewPool
        val childItemAdapter = parentItem.tricks.let {
            CourseListTrickAdapter(
                it
            )
        }

        parentViewHolder.ChildRecyclerView.layoutManager = layoutManager
        parentViewHolder.ChildRecyclerView.adapter = childItemAdapter
        parentViewHolder.ChildRecyclerView
            .setRecycledViewPool(viewPool)
        parentViewHolder.itemView.setOnClickListener { clickListener(parentItem, position) }
        parentViewHolder.itemView.findViewById<ImageView>(R.id.imageStar1).setOnClickListener {
            clickListener(parentItem, position)
            parentViewHolder.itemView.findViewById<ImageView>(R.id.imageStar1).setImageResource(R.drawable.img_star4)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ParentViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var ParentItemTitle: TextView = itemView
            .findViewById(
                R.id.course_name
            )
        var ParentItemLevel: TextView = itemView
            .findViewById(
                R.id.course_level
            )
        var ChildRecyclerView: RecyclerView = itemView
            .findViewById(
                R.id.course_list_items
            )
        var star: ImageView = itemView
            .findViewById(
                R.id.imageStar1
            )

    }
    }