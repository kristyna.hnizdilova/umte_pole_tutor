package cz.uhk.fim.umte.poletutor.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cz.uhk.fim.umte.poletutor.R
import cz.uhk.fim.umte.poletutor.dto.Trick


class CourseListTrickAdapter(
    private val mList: List<Trick>
) : RecyclerView.Adapter<CourseListTrickAdapter.ChildViewHolder>() {

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): ChildViewHolder {
        val view: View = LayoutInflater
            .from(viewGroup.context)
            .inflate(
                R.layout.course_list_item,
                viewGroup, false
            )
        return ChildViewHolder(view)
    }

    override fun onBindViewHolder(
        childViewHolder: ChildViewHolder,
        position: Int
    ) {
        val childItem: Trick = mList[position]
        childViewHolder.ChildItemTitle
            .setText(childItem.name)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ChildViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ChildItemTitle: TextView = itemView.findViewById(
            R.id.trick_name
        )

    }
}