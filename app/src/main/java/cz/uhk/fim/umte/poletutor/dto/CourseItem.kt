package cz.uhk.fim.umte.poletutor.dto

class CourseItem(
    var courseName: String,
    var level: String,
    var tricks: List<Trick>,
    var id: String
)