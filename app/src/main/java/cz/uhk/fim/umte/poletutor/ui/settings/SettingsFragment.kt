package cz.uhk.fim.umte.poletutor.ui.settings

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import cz.uhk.fim.umte.poletutor.HomeActivity
import cz.uhk.fim.umte.poletutor.R
import cz.uhk.fim.umte.poletutor.databinding.FragmentHomeBinding

class SettingsFragment : Fragment() {
    private lateinit var db: FirebaseFirestore
    private var _binding: FragmentHomeBinding? = null
    private var ha: HomeActivity? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        db = FirebaseFirestore.getInstance()
        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        val button = view.findViewById<Button>(R.id.btnOk)
        val name = view.findViewById<EditText>(R.id.etJmnoAPjmen)
        button.setOnClickListener {
            val nameData = name.text.toString()

            if (nameData.isBlank()) {
                Toast.makeText(context, "Jméno musí být vyplněné", Toast.LENGTH_SHORT).show()
            }
            db.collection("users")
                .document(FirebaseAuth.getInstance().currentUser!!.uid)
                .update("name", nameData)
                .addOnSuccessListener {
                    Toast.makeText(context, "Jméno úspěšně změněno", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(context,it.message.toString(),Toast.LENGTH_LONG).show()
                }
        }

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}