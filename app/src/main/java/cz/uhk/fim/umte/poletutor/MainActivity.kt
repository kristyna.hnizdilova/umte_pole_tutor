package cz.uhk.fim.umte.poletutor
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import cz.uhk.fim.umte.poletutor.ui.signIn.SigninActivity


class MainActivity : AppCompatActivity() {

    private lateinit var tvRedirectSignUp: TextView
    lateinit var etEmail: EditText
    private lateinit var etPass: EditText
    lateinit var btnLogin: Button

    // Creating firebaseAuth object
    lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_screen)
        // View Binding
        tvRedirectSignUp = findViewById(R.id.txtRegistrace)
        btnLogin = findViewById(R.id.btnPihlsitSe)
        etEmail = findViewById(R.id.etEmail)
        etPass = findViewById(R.id.etHeslo)

        // initialising Firebase auth object
        auth = FirebaseAuth.getInstance()

        btnLogin.setOnClickListener {
            login()
        }

        tvRedirectSignUp.setOnClickListener {
            val intent = Intent(this, SigninActivity::class.java)
            startActivity(intent)
            finish()
        }
    }


        private fun login() {
            val email = etEmail.text.toString()
            val pass = etPass.text.toString()
            // calling signInWithEmailAndPassword(email, pass)
            if (email.isBlank() || pass.isBlank()) {
                Toast.makeText(this, "Email a heslo musí být vyplněné", Toast.LENGTH_SHORT).show()
                return
            }

            if (email.length < 6 || pass.length < 6) {
                Toast.makeText(
                    this,
                    "Email a heslo musí být alespoň 6 znaků dlouhé",
                    Toast.LENGTH_SHORT).show()
                    return
            }

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(
                    this,
                    "Zadejte prosím validní emailovou adresu viz. example@gmail.com",
                    Toast.LENGTH_SHORT
                ).show()
                return
            }


            auth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(this) {
                if (it.isSuccessful) {
                    try {
                    Toast.makeText(this, "Uživatel úspěšně přihlášen", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                    }
                    catch (e: FirebaseAuthInvalidCredentialsException) {
                        Toast.makeText(this, it.result.toString(), Toast.LENGTH_SHORT).show()

                    }
                } else {
                    Toast.makeText(this, it.result.toString(), Toast.LENGTH_SHORT).show()
                    return@addOnCompleteListener
                }
            }
        }




    /** Called when the user taps the Send button */
    fun sendMessage(view: View) {
        }
}