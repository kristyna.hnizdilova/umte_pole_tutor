package cz.uhk.fim.umte.poletutor.dto

data class Trick (
    var name: String,
    var id: String,
    var level: String,
    var description: String,
    var favourite: Boolean?,
    var hate: Boolean?,
    var image: String
    )