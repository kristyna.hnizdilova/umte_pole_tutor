package cz.uhk.fim.umte.poletutor.ui.trickDetail

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ListResult
import com.google.firebase.storage.ktx.storage
import cz.uhk.fim.umte.poletutor.R
import java.io.FileNotFoundException
import java.io.IOException


class TrickDetailFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private lateinit var name: String
    private lateinit var id: String
    private lateinit var level: String
    private lateinit var description: String
    private lateinit var improve: String
    private lateinit var favourite: String
    private lateinit var courseId: String
    private lateinit var imageUri: Uri
    private lateinit var myImageView: ImageView
    private val thumbnail: Bitmap? = null

    val permissions = arrayOf(Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE)

    val storage = Firebase.storage.reference
    lateinit var images: Task<ListResult>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        name = arguments?.getString("name").toString()
        id = arguments?.getString("id").toString()
        level = arguments?.getString("lvl").toString()
        description = arguments?.getString("desc").toString()
        improve = arguments?.getString("h").toString()
        favourite = arguments?.getString("f").toString()
        courseId = arguments?.getString("cId").toString()
        //imageUri = arguments?.getString("im").toString()
        db = FirebaseFirestore.getInstance()

        val view = inflater.inflate(R.layout.fragment_trick_detail, container, false)

        //set name
        val title: TextView = view.findViewById(R.id.trick_name)
        title.setText(name)

        //set description
        val desc: TextView = view.findViewById(R.id.postup)
        desc.setText(description)

        myImageView = view.findViewById(R.id.trickimage)

        //level logic
        when (level) {
            "1" -> view.findViewById<ImageView>(R.id.st1).setImageResource(R.drawable.img_star4)
            "2" -> { view.findViewById<ImageView>(R.id.st1).setImageResource(R.drawable.img_star4)
                  view.findViewById<ImageView>(R.id.st2).setImageResource(R.drawable.img_star4)}
            "3" -> { view.findViewById<ImageView>(R.id.st1).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st2).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st3).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st4).setImageResource(R.drawable.img_star4)}
            "4" -> { view.findViewById<ImageView>(R.id.st1).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st2).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st3).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st4).setImageResource(R.drawable.img_star4)}
            "5" -> { view.findViewById<ImageView>(R.id.st1).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st2).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st3).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st4).setImageResource(R.drawable.img_star4)
                view.findViewById<ImageView>(R.id.st5).setImageResource(R.drawable.img_star4)}
            else -> {
                print("x is neither 1 nor 2")
            }
        }

        //set image
        val parentRecyclerViewItem: RecyclerView = view.findViewById(R.id.photos)
        val layoutManager = LinearLayoutManager(
            this.context
        )

        //favourite logic
        val star: ImageView = view.findViewById(R.id.favstar)
        if(favourite == "true")
            star.setImageResource(R.drawable.img_star1)

        star.setOnClickListener{
            setFavourite()
            if(favourite == "true") {
                favourite = "false"
                star.setImageResource(R.drawable.img_star2)}
            else
            {   favourite = "true"
                star.setImageResource(R.drawable.img_star1)}
        }

        //improve logic
        val star1: ImageView = view.findViewById(R.id.hatestar)
        if(improve == "true")
            star1.setImageResource(R.drawable.img_star1)
        star1.setOnClickListener{
            setImprove()
            if(improve == "true")
            {improve = "false"
                star1.setImageResource(R.drawable.img_star2)}
            else
            {  improve = "true"
                star1.setImageResource(R.drawable.img_star1)}
        }

        //Camera
        val photoButton: Button = view.findViewById(R.id.takephoto)
        photoButton.setOnClickListener {
            takePhoto(it)
        }

       // parentRecyclerViewItem.adapter = listFiles()
        parentRecyclerViewItem.layoutManager = layoutManager
        return view
    }

    private fun takePhoto(it: View) {
        if (hasNoPermissions()) {
            requestPermission()
        }else{
                val myImageview = it.findViewById<ImageView>(R.id.trickimage)
                val values = ContentValues()
                values.put(MediaStore.Images.Media.TITLE, "MyPicture")

                imageUri = context?.contentResolver?.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    values
                )!!
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(intent, 123)
        }


    }

    //add to favoure
    private fun setFavourite() {
        db.collection("users")
            .document(FirebaseAuth.getInstance().currentUser!!.uid)
            .collection("courses")
            .document(courseId)
            .collection("tricks")
            .document(id)
            .update("favourite", favourite != "true")
    }

    //add to improve
    private fun setImprove() {
        db.collection("users")
            .document(FirebaseAuth.getInstance().currentUser!!.uid)
            .collection("courses")
            .document(courseId)
            .collection("tricks")
            .document(id)
            .update("hate", improve != "true")
    }



    private fun hasNoPermissions(): Boolean{
        return context?.let {
            ContextCompat.checkSelfPermission(
                it,
                Manifest.permission.READ_EXTERNAL_STORAGE)
        } != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
    }

    fun requestPermission(){
        requestPermissions(permissions,0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === 123) {
            val bitmap: Bitmap
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context?.getContentResolver(), imageUri)
                myImageView?.setImageBitmap(bitmap)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    /* from camera
    private fun uploadImageToStorage(filename: String) = CoroutineScope(Dispatchers.IO).launch {
        try {
            curFile?.let {
                imageRef.child("images/$filename").putFile(it).await()
                withContext(Dispatchers.Main) {
                    Toast.makeText(this@MainActivity, "Successfully uploaded image",
                        Toast.LENGTH_LONG).show()
                }
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                Toast.makeText(this@MainActivity, e.message, Toast.LENGTH_LONG).show()
            }
        }
    }*/

}