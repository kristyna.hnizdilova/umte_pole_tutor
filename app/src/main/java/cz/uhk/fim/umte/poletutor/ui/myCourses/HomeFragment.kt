package cz.uhk.fim.umte.poletutor.ui.myCourses

import cz.uhk.fim.umte.poletutor.adapters.CourseListAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import cz.uhk.fim.umte.poletutor.R
import cz.uhk.fim.umte.poletutor.databinding.FragmentHomeBinding
import cz.uhk.fim.umte.poletutor.dto.CourseItem
import cz.uhk.fim.umte.poletutor.dto.Trick

class HomeFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private var _binding: FragmentHomeBinding? = null
    private lateinit var parentItemAdapter: CourseListAdapter
    private lateinit var noCourses: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        db = FirebaseFirestore.getInstance()
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        val parentRecyclerViewItem: RecyclerView = view.findViewById(R.id.course_list)
        val layoutManager = LinearLayoutManager(
            this.context
        )

        parentItemAdapter = getCourses()
        if (parentItemAdapter.itemCount == 0)
            noCourses = view.findViewById(R.id.nocourses)
            noCourses.text = "Nemáte zapsané žádné kurzy. Vyberte si z nabídky kurzů v menu"


        parentRecyclerViewItem.adapter = parentItemAdapter
        parentRecyclerViewItem.layoutManager = layoutManager

        return view
    }

    private fun getCourses(): CourseListAdapter {
        val list: MutableList<CourseItem> = ArrayList()
        db.collection("users")
            .document(FirebaseAuth.getInstance().currentUser!!.uid)
            .collection("courses")
            .get()
            .addOnSuccessListener { task ->
                for (document in task) {
                    list.add(
                        CourseItem(
                        document.get("name").toString(),
                        document.get("level").toString(),
                        getTricks(document.id),
                        document.id
                    )
                    )
                }
                parentItemAdapter.notifyDataSetChanged()
            }
        return CourseListAdapter(getMyCourses(),list){ itemDto: CourseItem, position: Int ->
            event(itemDto)
        }
    }

    private fun event(itemDto: CourseItem) {
        val bundle = Bundle()
        bundle.putString("courseName", itemDto.courseName)
        bundle.putString("id", itemDto.id)
        val act = activity?.findNavController(R.id.nav_host_fragment_content_home)
        act?.navigate(R.id.courseDetailFragment, bundle)
    }

    private fun getTricks(id: String): List<Trick> {
        val list: MutableList<Trick> = ArrayList()
        db.collection("users")
            .document(FirebaseAuth.getInstance().currentUser!!.uid)
            .collection("courses")
            .document(id)
            .collection("tricks")
            .get()
            .addOnSuccessListener{ task ->
                for (document in task) {
                    list.add(
                        Trick(
                            document.get("name").toString(),
                            document.id,
                            document.get("level").toString(),
                            document.get("description").toString(),
                            document.getBoolean("favourite"),
                            document.getBoolean("hate"),
                            document.get("image").toString()

                        )
                    )
                }
                parentItemAdapter.notifyDataSetChanged()
            }

        return list
    }

    private fun getMyCourses(): List<String> {
        val list: MutableList<String> = ArrayList()
        db.collection("users")
            .document(FirebaseAuth.getInstance().currentUser!!.uid)
            .collection("courses")
            .get()
            .addOnSuccessListener { task ->
                for (document in task) {
                    list.add(
                        document.id
                    )
                }
            }
        return list
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}