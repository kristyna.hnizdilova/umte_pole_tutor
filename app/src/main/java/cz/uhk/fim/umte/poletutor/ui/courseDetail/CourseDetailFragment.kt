package cz.uhk.fim.umte.poletutor.ui.courseDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import cz.uhk.fim.umte.poletutor.R
import cz.uhk.fim.umte.poletutor.adapters.TrickListAdapter
import cz.uhk.fim.umte.poletutor.dto.CourseItem
import cz.uhk.fim.umte.poletutor.dto.Trick

class CourseDetailFragment : Fragment() {

    private lateinit var courseName: String
    private lateinit var courseId: String
    private lateinit var db: FirebaseFirestore
    private lateinit var itemAdapter: TrickListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        courseName = arguments?.get("courseName").toString()
        courseId = arguments?.get("id").toString()

        db = FirebaseFirestore.getInstance()
        val view = inflater.inflate(R.layout.fragment_course_detail, container, false)
        val title: TextView = view.findViewById(R.id.title)
        title.setText(courseName)

        val parentRecyclerViewItem: RecyclerView = view.findViewById(R.id.trick_list)
        val layoutManager = LinearLayoutManager(
            this.context
        )
        itemAdapter = getTricks()
        parentRecyclerViewItem.adapter = itemAdapter
        parentRecyclerViewItem.layoutManager = layoutManager

        return view
    }

    private fun getTricks(): TrickListAdapter {
        val list: MutableList<Trick> = ArrayList()
        db.collection("users")
            .document(FirebaseAuth.getInstance().currentUser!!.uid)
            .collection("courses")
            .document(courseId)
            .collection("tricks")
            .get()
            .addOnSuccessListener { task ->
               for (document in task) {
                   list.add(
                        Trick(
                            document.get("name").toString(),
                            document.id,
                            document.get("level").toString(),
                            document.get("description").toString(),
                            document.getBoolean("favourite"),
                            document.getBoolean("hate"),
                            document.get("image").toString()
                        )
                   )
               }
            itemAdapter.notifyDataSetChanged()
            }
        return TrickListAdapter(list) {itemDto: Trick, position: Int ->
            event(itemDto)
        }
        }

        private fun event(itemDto: Trick) {
            val bundle = Bundle()
            bundle.putString("name", itemDto.name)
            bundle.putString("id", itemDto.id)
            bundle.putString("desc", itemDto.description)
            bundle.putString("lvl", itemDto.level)
            bundle.putString("h", itemDto.hate.toString())
            bundle.putString("f", itemDto.favourite.toString())
            bundle.putString("cId", courseId)
            bundle.putString("im", itemDto.image)
            val act = activity?.findNavController(R.id.nav_host_fragment_content_home)
            act?.navigate(R.id.trickDetailFragment, bundle)
        }

}