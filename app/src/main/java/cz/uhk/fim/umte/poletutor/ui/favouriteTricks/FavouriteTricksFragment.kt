package cz.uhk.fim.umte.poletutor.ui.favouriteTricks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import cz.uhk.fim.umte.poletutor.R
import cz.uhk.fim.umte.poletutor.adapters.TrickListAdapter
import cz.uhk.fim.umte.poletutor.databinding.FragmentHomeBinding
import cz.uhk.fim.umte.poletutor.dto.Trick

class FavouriteTricksFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private var _binding: FragmentHomeBinding? = null
    lateinit var itemAdapter: TrickListAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        db = FirebaseFirestore.getInstance()
        val view = inflater.inflate(R.layout.fragment_fav_hate_tricks, container, false)
        val title: TextView = view.findViewById(R.id.title)
        title.setText("Oblíbené triky")
        val parentRecyclerViewItem: RecyclerView = view.findViewById(R.id.trick_list)
        val layoutManager = LinearLayoutManager(
            this.context
        )
        itemAdapter = getTricks()
        parentRecyclerViewItem.adapter = itemAdapter
        parentRecyclerViewItem.layoutManager = layoutManager

        return view
    }

    private fun getTricks(): TrickListAdapter {
        val list: MutableList<Trick> = ArrayList()
        db.collection("users")
            .document(FirebaseAuth.getInstance().currentUser!!.uid)
            .collection("courses")
            .get()
            .addOnSuccessListener { task ->
                for (document in task) {
                    db.collection("users")
                        .document(FirebaseAuth.getInstance().currentUser!!.uid)
                        .collection("courses")
                        .document(document.id)
                        .collection("tricks")
                        .get()
                        .addOnSuccessListener { t ->
                            for (document in t) {
                                if(document.getBoolean("favourite") == true){
                                    list.add(
                                        Trick(
                                            document.get("name").toString(),
                                            document.id,
                                            document.get("level").toString(),
                                            document.get("description").toString(),
                                            document.getBoolean("favourite"),
                                            document.getBoolean("hate"),
                                            document.get("image").toString()
                                        ))
                                }

                            }
                            itemAdapter.notifyDataSetChanged()
                        }
                        .addOnFailureListener { Toast.makeText(context, it.message.toString(),Toast.LENGTH_LONG).show() }
                }
                itemAdapter.notifyDataSetChanged()
            }
        return TrickListAdapter(list) {itemDto: Trick, position: Int ->
            event(itemDto)
        }
    }

    private fun event(itemDto: Trick) {
        val bundle = Bundle()
        bundle.putString("name", itemDto.name)
        bundle.putString("id", itemDto.id)
        bundle.putString("desc", itemDto.description)
        bundle.putString("lvl", itemDto.level)
        bundle.putString("h", itemDto.hate.toString())
        bundle.putString("f", itemDto.favourite.toString())
        bundle.putString("cId", getCourse(itemDto.id))
        bundle.putString("im", itemDto.image)
        val act = activity?.findNavController(R.id.nav_host_fragment_content_home)
        act?.navigate(R.id.trickDetailFragment, bundle)
    }
    private fun getCourse(id: String): String {
        var course = ""
        db.collection("users")
            .document(FirebaseAuth.getInstance().currentUser!!.uid)
            .collection("courses")
            .get()
            .addOnSuccessListener{ task ->
                for(document in task){
                    db.collection("users")
                        .document(FirebaseAuth.getInstance().currentUser!!.uid)
                        .collection("courses")
                        .document(document.id)
                        .collection("tricks")
                        .get()
                        .addOnSuccessListener { t ->
                            for (d in t) {
                                if(d.id == id){
                                    course = document.id
                                }

                            }
                        }
                }
            }
        return course
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}